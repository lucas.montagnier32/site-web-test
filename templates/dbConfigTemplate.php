<?php
return array(
    'host' => "{{ db_host }}",
    'username' => "{{ db_user }}",
    'password' => "{{ db_pass }}",
    'dbname' => "{{ db_name }}",
);
<?php
require_once('../service/ProductService.php');

$bddService = ProductService::getInstance();

$id = 1;

$products = $bddService->searchById($id);
$productsInfo = $products->fetch(PDO::FETCH_ASSOC);
$rowCount = $products->rowCount();

// verification de résultat
echo "nb rows: " . $rowCount . "\n";
if ($rowCount>0){
    echo "product name: " . $productsInfo['name'] . "\n";
    echo "product description: " . $productsInfo['description'] . "\n";
}
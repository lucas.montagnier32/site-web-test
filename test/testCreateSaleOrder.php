<?php
require_once('../service/SaleOrderService.php');

$bddService = SaleOrderService::getInstance();

$userId = 2;

$productsInShoppingCart = array();
$productsInShoppingCart[0] = array();
$productsInShoppingCart[0]["demand_quantity"] = 3;
//$productsInShoppingCart[0]["price_ht"] = 9.99;

try {
    $bddService->createSaleOrder($userId, $productsInShoppingCart);
    
    echo "ok \n";
} catch (Exception $e) {
    $e->getTraceAsString();
    echo $e . "\n";
}
<?php
require_once('../service/UserService.php');

$bddService = UserService::getInstance();

$data = array();
$data['mail'] = 'test@test.com';
$data['password'] = '1234';
$data['firstname'] = 'firstname';
$data['lastname'] = 'lastname';
$data['address'] = 'address';
$data['zipcode'] = 'zipcode';
//$data['city'] = 'city';

try {
    $user = $bddService->saveAccount($data);
    
    $userInfo = $user->fetch(PDO::FETCH_ASSOC);

    // verification de résultat
    echo "user created\n";
    echo $userInfo['email'] . " " . $userInfo['password'] . "\n";
    
} catch (Exception $e) {
    $e->getTraceAsString();
    echo $e . "\n";
}


<?php
require_once('../service/ProductService.php');

$bddService = ProductService::getInstance();

$name = "table";

$products = $bddService->searchByName($name);
$productsInfo = $products->fetch(PDO::FETCH_ASSOC);
$rowCount = $products->rowCount();

// verification de résultat
echo "nb rows: " . $rowCount . "\n";
if ($rowCount>0){
    echo "first product name: " . $productsInfo['name'] . "\n";
    echo "first product description: " . $productsInfo['description'] . "\n";
}
<?php
require_once('../service/ProductService.php');

$bddService = ProductService::getInstance();


$products = $bddService->selectTwoProduct();

echo "rowcount: " . $products->rowCount() . "\n";

$p1 = $products->fetch(PDO::FETCH_ASSOC);
$p2 = $products->fetch(PDO::FETCH_ASSOC);

// verification de résultat
echo "first product: " . $p1['id'] . " : " . $p1['name'] . "\n";
echo "second product : " . $p2['id'] . " : " . $p2['name'] . "\n";
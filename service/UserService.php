<?php
require_once("Logger.php");
require_once("BddService.php");

class UserService extends BddService
{
    
    private static ?UserService $instance = null;
    
    private function __construct(){}
    
    /**
     * @return UserService
     */
    public static function getInstance(): UserService
    {
        if(is_null(self::$instance)) {
            self::$instance = new UserService();
        }
        
        return self::$instance;
    }
    
    /**
     * Inserts a customer account in the database and returns the inserted line.
     *
     * @param array<string,string> $data
     * @return PDOStatement
     * @throws Exception
     */
    public function saveAccount(array $data): PDOStatement
    {
        $verify = array('mail', 'password', 'firstname', 'lastname', 'address', 'zipcode', 'city');
        
        foreach ($verify as $str){
            if (!isset($data[$str])) {
                throw new Exception("Error: missing " . $str);
            }
        }
        
        $pdo = $this->dbconnect();
    
        Logger::debug("saveAccount for " . $data['mail']);
        
        $mail = $data['mail'];
        $userPassword = $data['password'];
        $firstname = $data['firstname'];
        $lastname = $data['lastname'];
        $address = $data['address'];
        $zipcode = $data['zipcode'];
        $city = $data['city'];
        
        $sql = "INSERT INTO user VALUES(null, '$mail', '$userPassword', '$firstname', '$lastname', '$address', '$zipcode', '$city')";
        $sql2 = "SELECT * FROM user WHERE email='$mail' AND password='$userPassword'";
        
        $pdo->exec($sql);
        
        $newUser = $pdo->query($sql2);
    
        if($newUser === false){
            throw new Exception("database query error");
        }
    
        return $newUser;
    }
    
    /**
     * Search in the database for an account using an email and a password.
     *
     * @param string $mail
     * @param string $userPassword
     * @return PDOStatement
     */
    public function searchAccount(string $mail, string $userPassword): PDOStatement
    {
        $pdo = $this->dbconnect();
        
        $sql = "SELECT * FROM user WHERE email='$mail' AND password='$userPassword'";
    
        Logger::debug("searchAccount for " . $mail);
    
        $newUser = $pdo->query($sql);
    
        if($newUser === false){
            throw new Exception("database query error");
        }
    
        return $newUser;
    }
}
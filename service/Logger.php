<?php
require_once("Config.php");

class Logger
{
//emergency(...) équivalent à log(LogLevel::EMERGENCY, ...)
//alert(...) équivalent à log(LogLevel::ALERT, ...)
//critical(...) équivalent à log(LogLevel::CRITICAL, ...)
//error(...) équivalent à log(LogLevel::ERROR, ...)
//warning(...) équivalent à log(LogLevel::WARNING, ...)
//notice(...) équivalent à log(LogLevel::NOTICE, ...)
//info(...) équivalent à log(LogLevel::INFO, ...)
//debug(...) équivalent à log(LogLevel::DEBUG, ...)
    
    /**
     * @param string $message
     * @return void
     * @throws Exception
     */
    static function debug(string $message)
    {
        $config = Config::getLogConfig();
        $message = "[DEBUG] " . $message . "\n";
        $logfile = $config['logPath'];
        if(!file_put_contents($logfile,$message,FILE_APPEND)){
            throw new Exception("log error");
        }
    }
    
    /**
     * @return void
     * @throws Exception
     */
    static function emptyLog()
    {
        $config = Config::getLogConfig();
        $logfile = $config['logPath'];
        $file = fopen($logfile,'w');
        if($file === false){
            throw new Exception("log error");
        }
        fclose($file);
    }
}
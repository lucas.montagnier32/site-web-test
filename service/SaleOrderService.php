<?php
require_once("Logger.php");
require_once("BddService.php");

class SaleOrderService extends BddService
{
    private static ?SaleOrderService $instance = null;
    
    private function __construct(){}
    
    /**
     * @return SaleOrderService
     */
    public static function getInstance(): SaleOrderService
    {
        if(is_null(self::$instance)) {
            self::$instance = new SaleOrderService();
        }
        
        return self::$instance;
    }
    
    /**
     * Inserts the customer's sales order and order items into the database
     * based on the user's info and cart saved in session.
     *
     * @param int $user_id
     * @param array<int,array<string,string>> $productsInShoppingCart
     * @return void
     * @throws Exception
     */
    public function createSaleOrder(int $user_id, array $productsInShoppingCart)
    {
        // vérifications
        if (count($productsInShoppingCart) <= 0){
            throw new Exception("Error: The shopping cart is empty \n");
        }
        foreach ($productsInShoppingCart as $key => $product) {
            if ( !isset($product['demand_quantity']) || !isset($product['price_ht']) ) {
                throw new Exception("Error: Missing information in shopping cart \n");
            }
        };
        
        $pdo = $this->dbconnect();
    
        Logger::debug("createSaleOrder");
        
        $sql = "INSERT INTO sale_order VALUES(null, CURDATE() , $user_id)";
        
        $pdo->exec($sql);
        
        /* création des order_item liée a sale_order */
        $sale_order_id = $pdo->lastInsertId();
        
        foreach ($productsInShoppingCart as $key => $product) {
            $demand_quantity = $product['demand_quantity'];
            $price_ht = $product['price_ht'];
            $sqlInsert = "INSERT INTO order_item VALUES(null, $sale_order_id, $key, $demand_quantity, $price_ht)";
            $sqlUpdate = "UPDATE product SET quantity = (quantity-$demand_quantity) WHERE id=$key";
            
            $pdo->exec($sqlInsert);
            $pdo->exec($sqlUpdate);
        };
    }
}
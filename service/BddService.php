<?php
require_once("Logger.php");
require_once("Config.php");

class BddService
{
    
    /**
     * Create a connection to the database.
     *
     * @return PDO
     */
    protected function dbconnect(): PDO
    {
        $config = Config::getDbConfig();
        $host = $config['host'];
        $dbname = $config['dbname'];
        $username = $config['username'];
        $password = $config['password'];
    
        $dsn = "mysql:host=$host;dbname=$dbname";
        
        Logger::debug("connecting to database " . $dbname . " with host: " . $host . ", username: " . $username);
        
        $pdo = new PDO($dsn, $username, $password);
    
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    
        return $pdo;
    }
    
}
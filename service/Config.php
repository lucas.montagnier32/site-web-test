<?php

class Config
{
    /**
     * @return array<string,string>
     */
    static function getDbConfig(): array
    {
        return include('../config/dbConfig.php');
    }
    
    /**
     * @return array<string,string>
     */
    static function getLogConfig(): array
    {
        return include('../config/logConfig.php');
    }
}
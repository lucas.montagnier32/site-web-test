<?php
require_once("Logger.php");
require_once("BddService.php");

class ProductService extends BddService
{
    private static ?ProductService $instance = null;
    
    private function __construct(){}
    
    /**
     * @return ProductService
     */
    public static function getInstance(): ProductService
    {
        if(is_null(self::$instance)) {
            self::$instance = new ProductService();
        }
        
        return self::$instance;
    }
    
    /**
     * Gives all the products in the database.
     *
     * @return PDOStatement
     */
    public function getAllProducts(): PDOStatement
    {
        $pdo = $this->dbconnect();
    
        Logger::debug("getAllProducts");
        
        $sql = "SELECT * FROM product WHERE is_active=true AND quantity>0 ORDER BY name";

        $products = $pdo->query($sql);
        
        if($products === false){
            throw new Exception("database query error");
        }

        return $products;
    }
    
    /**
     * Gives all the products in the database based on customer's choice.
     *
     * @param int $priceId Id used to determine the customer's choice.
     * @param string $by Name of the attribute to sort on.
     * @return PDOStatement
     */
    public function searchByPriceAndSort(int $priceId, string $by): PDOStatement
    {
        $pdo = $this->dbconnect();
        
        switch ($priceId) {
            case 1:
                $sql = "SELECT * FROM product WHERE price_ht<=5 AND is_active=true AND quantity>0 ORDER BY $by";
                break;
            case 2:
                $sql = "SELECT * FROM product WHERE price_ht<=20 AND is_active=true AND quantity>0 ORDER BY $by";
                break;
            case 3:
                $sql = "SELECT * FROM product WHERE price_ht<=50 AND is_active=true AND quantity>0 ORDER BY $by";
                break;
            case 4:
                $sql = "SELECT * FROM product WHERE price_ht<=200 AND is_active=true AND quantity>0 ORDER BY $by";
                break;
            case 5:
                $sql = "SELECT * FROM product WHERE price_ht<=500 AND is_active=true AND quantity>0 ORDER BY $by";
                break;
            default:
                $sql = "SELECT * FROM product WHERE is_active=true AND quantity>0 ORDER BY $by";
        }
        Logger::debug("searchByPriceAndSort: " . $sql);
        
        $products = $pdo->query($sql);
    
        if($products === false){
            throw new Exception("database query error");
        }
        
        return $products;
    }
    
    /**
     * Gives all the products in the database based on customer's input name.
     *
     * @param string $name Name entered by the customer.
     * @return PDOStatement
     */
    public function searchByName(string $name): PDOStatement
    {
        $pdo = $this->dbconnect();

        //retourne les description contenant le mot $name
        $sql = "SELECT * FROM product WHERE (name='$name' OR LOCATE('$name',description)) AND is_active=true AND quantity>0 ORDER BY name";
    
        Logger::debug("searchByName: " . $sql);
        
        $products = $pdo->query($sql);
    
        if($products === false){
            throw new Exception("database query error");
        }
        
        return $products;
    }
    
    /**
     * Gives all the products in the database based on product id.
     *
     * @param int $id Product id.
     * @return PDOStatement
     */
    public function searchById(int $id): PDOStatement
    {
        $pdo = $this->dbconnect();
        
        //retourne les description contenant le mot $name
        $sql = "SELECT * FROM product WHERE id=$id";
    
        Logger::debug("searchById: " . $sql);
        
        $products = $pdo->query($sql);
    
        if($products === false){
            throw new Exception("database query error");
        }
        
        return $products;
    }
    
    /**
     * Selects 2 different products randomly from the database.
     *
     * @return PDOStatement
     */
    public function selectTwoProduct(): PDOStatement
    {
        $pdo = $this->dbconnect();
        
        $sqlGet = "SELECT id FROM product WHERE is_active=true AND quantity>0";
        $res = $pdo->query($sqlGet);
    
        if($res === false){
            throw new Exception("database query error");
        }
        
        $idArray = $res->fetchAll();
    
        if($idArray == false){
            throw new Exception("fetchAll error");
        }

        $rand_keys = array_rand($idArray, 2); // retourne un tableau de 2 index (clé)
        
        $id1 = $idArray[$rand_keys[0]]['id'];
        $id2 = $idArray[$rand_keys[1]]['id'];
    
        $sql = "SELECT * FROM product WHERE id=$id1 OR id=$id2";
    
        Logger::debug("selectTwoProduct: " . $sql);
    
        $products = $pdo->query($sql);
    
        if($products === false){
            throw new Exception("database query error");
        }
    
        return $products;
    }
}
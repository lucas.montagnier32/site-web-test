<?php

require_once "../service/UserService.php";

class Login
{

    private static ?Login $instance = null;
    
    private function __construct()
    {
    }
    
    /**
     * @return Login
     */
    public static function getInstance(): Login
    {
        if (is_null(self::$instance)) {
            self::$instance = new Login();
        }
        
        return self::$instance;
    }
    
    /**
     * search an account in the database using an email and a password.
     *
     * @return void
     */
    public function login()
    {
        $bddService = UserService::getInstance();
        
        $accountCreated = false;
        $connected = false;
        $productsInShoppingCart = $_SESSION['productsInShoppingCart'];
        
        if (isset($_POST['log_account'])) {
            $mail = $_POST['mail'];
            $userPassword = $_POST['password'];
            
            $user = $bddService->searchAccount($mail, $userPassword);
            $rowCount = $user->rowCount();
            if ($rowCount > 0) {
                $userInfo = $user->fetch(PDO::FETCH_ASSOC);
                $_SESSION['userInfo'] = $userInfo;
            }
            $connected = $rowCount>0;
        }
        
        require('../view/cartCheckoutPage.php');
    }
}

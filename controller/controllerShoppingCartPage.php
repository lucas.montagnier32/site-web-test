<?php

class ShoppingCartPage
{

    private static ?ShoppingCartPage $instance = null;

    private function __construct()
    {
    }
    
    /**
     * @return ShoppingCartPage
     */
    public static function getInstance(): ShoppingCartPage
    {
        if (is_null(self::$instance)) {
            self::$instance = new ShoppingCartPage();
        }
        
        return self::$instance;
    }
    
    /**
     * Load the shopping cart page.
     *
     * @return void
     */
    public function shoppingCartPage()
    {
        $product_count = $_SESSION['count'] ?? 0;/* si isset($_SESSION['count']) alors $_SESSION['count'] sinon vaut 0 */
        
        $productsInShoppingCart = $_SESSION['productsInShoppingCart'] ?? array();
        
        require('../view/shoppingCartPage.php');
    }
}

<?php

require_once('../service/ProductService.php');

class SelectTwoProduct
{

    private static ?SelectTwoProduct $instance = null;
    
    private function __construct()
    {
    }
    
    /**
     * @return SelectTwoProduct
     */
    public static function getInstance(): SelectTwoProduct
    {
        if (is_null(self::$instance)) {
            self::$instance = new SelectTwoProduct();
        }
        
        return self::$instance;
    }
    
    /**
     * @return void
     */
    public function selectTwoProduct()
    {
        $bddService = ProductService::getInstance();
    
        $products = $bddService->selectTwoProduct();
    
        $p1 = $products->fetch(PDO::FETCH_ASSOC);
        $p2 = $products->fetch(PDO::FETCH_ASSOC);
    
        if (!is_array($p1)) {
            throw new Exception("p1 is not a array");
        }
        if (!is_array($p2)) {
            throw new Exception("p2 is not a array");
        }
        
        echo "<div>image</div>";
        echo "<div>" . $p1['name'] . "</div>";
        echo "<div>" . $p1['quantity'] . " pieces restantes </div>";
        echo "<div id='price'>" . $p1['price_ht'] . " € HT </div>";
        echo "<div> <form id='fiche_button' method='get' action='produit?'>";
        echo "<input type='hidden' name='product_id' value=" . $p1['id'] . ">";
        echo "<input id='product_sheet_input' type='submit' value='voir fiche'>";
        echo "</form> </div>\n";
        echo "<div>image</div>";
        echo "<div>" . $p2['name'] . "</div>";
        echo "<div>" . $p2['quantity'] . " pieces restantes </div>";
        echo "<div id='price'>" . $p2['price_ht'] . " € HT </div>";
        echo "<div> <form id='fiche_button' method='get' action='produit?'>";
        echo "<input type='hidden' name='product_id' value=" . $p2['id'] . ">";
        echo "<input id='product_sheet_input' type='submit' value='voir fiche'>";
        echo "</form> </div>\n";
    }
}

<?php

class HomePage
{

    private static ?HomePage $instance = null;
    
    private function __construct()
    {
    }
    
    /**
     * @return HomePage
     */
    public static function getInstance(): HomePage
    {
        if (is_null(self::$instance)) {
            self::$instance = new HomePage();
        }
        
        return self::$instance;
    }
    
    /**
     * Load home page.
     *
     * @return void
     */
    public function homePage()
    {
        require('../view/homePage.php');
    }
}

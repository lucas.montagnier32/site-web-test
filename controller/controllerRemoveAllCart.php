<?php

class RemoveAllCart
{

    private static ?RemoveAllCart $instance = null;
    
    private function __construct()
    {
    }
    
    /**
     * @return RemoveAllCart
     */
    public static function getInstance(): RemoveAllCart
    {
        if (is_null(self::$instance)) {
            self::$instance = new RemoveAllCart();
        }
        
        return self::$instance;
    }
    
    /**
     * Remove the customer shopping cart.
     *
     * @return void
     */
    public function removeAllShoppingCart()
    {
        $_SESSION['count'] = 0;
        $_SESSION['productsInShoppingCart'] = array();
        
        /* maj des variables utilisées pour l'affichage */
        $product_count = $_SESSION['count'];
        $productsInShoppingCart = $_SESSION['productsInShoppingCart'];
        
        require('../view/shoppingCartPage.php');
    }
}

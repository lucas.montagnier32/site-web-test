<?php

require_once "../service/SaleOrderService.php";

class ConfirmOrder
{

    private static ?ConfirmOrder $instance = null;
    
    private function __construct()
    {
    }
    
    /**
     * @return ConfirmOrder
     */
    public static function getInstance(): ConfirmOrder
    {
        if (is_null(self::$instance)) {
            self::$instance = new ConfirmOrder();
        }
        
        return self::$instance;
    }
    
    /**
     * Confirm and create an order in the database.
     *
     * @return void
     */
    public function confirmOrder()
    {
        $bddService = SaleOrderService::getInstance();
        
        $userInfo = $_SESSION['userInfo'];
        
        $user_id = $_SESSION['userInfo']['id'];
        $productsInShoppingCart = $_SESSION['productsInShoppingCart'];
    
        try {
            $bddService->createSaleOrder($user_id, $productsInShoppingCart);
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    
        /* vide le panier */
        $_SESSION['count'] = 0;
        $_SESSION['productsInShoppingCart'] = array();
        
        require('../view/orderConfirmationPage.php');
    }
}

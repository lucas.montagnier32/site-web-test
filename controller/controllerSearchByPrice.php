<?php

require_once "../service/ProductService.php";

class SearchByPrice
{

    private static ?SearchByPrice $instance = null;
    
    private function __construct()
    {
    }
    
    /**
     * @return SearchByPrice
     */
    public static function getInstance(): SearchByPrice
    {
        if (is_null(self::$instance)) {
            self::$instance = new SearchByPrice();
        }
        
        return self::$instance;
    }
    
    /**
     * Search in the database for products that match with the price and ordered by a parameter.
     *
     * @return void
     */
    public function searchByPrice()
    {
        $bddService = ProductService::getInstance();
        
        if (isset($_POST['submit'])) {
            $id = $_POST['prix'];
            $by = $_POST['sort'];
            $products = $bddService->searchByPriceAndSort($id, $by);
        }
        
        require('../view/searchPage.php');
    }
}

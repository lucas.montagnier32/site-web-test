<?php

require_once "../service/ProductService.php";

class SearchByName
{

    private static ?SearchByName $instance = null;
    
    private function __construct()
    {
    }
    
    /**
     * @return SearchByName
     */
    public static function getInstance(): SearchByName
    {
        if (is_null(self::$instance)) {
            self::$instance = new SearchByName();
        }
        
        return self::$instance;
    }
    
    /**
     * Search in the database for products that match the name entered.
     *
     * @return void
     */
    public function searchByName()
    {
        $bddService = ProductService::getInstance();
        
        if (isset($_POST['submit_name'])) {
            $name = $_POST['search_name'];
            $products = $bddService->searchByName($name);
        }
        
        require('../view/searchPage.php');
    }
}

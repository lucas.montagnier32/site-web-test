<?php

class RemoveProductFromCart
{

    private static ?RemoveProductFromCart $instance = null;
    
    private function __construct()
    {
    }
    
    /**
     * @return RemoveProductFromCart
     */
    public static function getInstance(): RemoveProductFromCart
    {
        if (is_null(self::$instance)) {
            self::$instance = new RemoveProductFromCart();
        }
        
        return self::$instance;
    }
    
    /**
     * Remove a product from the customer shopping cart.
     *
     * @return void
     */
    public function removeProductFromShoppingCart()
    {
        
        /* maj de la sauvegarde en session du panier */
        $_SESSION['count'] -= $_SESSION['productsInShoppingCart'][$_GET['array_index_to_remove']]["demand_quantity"];
        unset($_SESSION['productsInShoppingCart'][$_GET['array_index_to_remove']]);
        
        /* maj des variables utilisées pour l'affichage */
        $product_count = $_SESSION['count'];
        $productsInShoppingCart = $_SESSION['productsInShoppingCart'];
        
        require('../view/shoppingCartPage.php');
    }
}

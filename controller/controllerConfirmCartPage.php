<?php

class ConfirmCartPage
{
    
    private static ?ConfirmCartPage $instance = null;
    
    private function __construct()
    {
    }
    
    /**
     * @return ConfirmCartPage
     */
    public static function getInstance(): ConfirmCartPage
    {
        if (is_null(self::$instance)) {
            self::$instance = new ConfirmCartPage();
        }
        
        return self::$instance;
    }
    
    /**
     * Load the cart checkout page.
     *
     * @return void
     */
    public function toConfirmCart()
    {
        $accountCreated = false;
        
        $productsInShoppingCart = $_SESSION['productsInShoppingCart'];
        
        if (isset($_SESSION['userInfo'])) {
            $userInfo = $_SESSION['userInfo'];
        }
        $connected = isset($_SESSION['userInfo']);
        
        require('../view/cartCheckoutPage.php');
    }
}

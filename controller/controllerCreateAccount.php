<?php

require_once "../service/UserService.php";

class CreateAccount
{

    private static ?CreateAccount $instance = null;
    
    private function __construct()
    {
    }
    
    /**
     * @return CreateAccount
     */
    public static function getInstance(): CreateAccount
    {
        if (is_null(self::$instance)) {
            self::$instance = new CreateAccount();
        }
        
        return self::$instance;
    }
    
    /**
     * Create an account in the database.
     *
     * @return void
     */
    public function createAccount()
    {
        $bddService = UserService::getInstance();
        
        $accountCreated = false;
        $connected = false;
        $productsInShoppingCart = $_SESSION['productsInShoppingCart'];
        
        if (isset($_POST['create_account'])) {
            $data = array();
            $data['mail'] = $_POST['mail'];
            $data['password'] = $_POST['password'];
            $data['firstname'] = $_POST['firstname'];
            $data['lastname'] = $_POST['lastname'];
            $data['address'] = $_POST['address'];
            $data['zipcode'] = $_POST['zipcode'];
            $data['city'] = $_POST['city'];
            
            try {
                $user = $bddService->saveAccount($data);
                $userInfo = $user->fetch(PDO::FETCH_ASSOC);
                $accountCreated = true;
                $connected = true;
                $_SESSION['userInfo'] = $userInfo;
            } catch (Exception $e) {
                echo $e->getMessage();
            }
        }
        
        require('../view/cartCheckoutPage.php');
    }
}

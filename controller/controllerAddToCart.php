<?php

require_once "../service/ProductService.php";

class AddToCart
{
    private static ?AddToCart $instance = null;
    
    private function __construct()
    {
    }
    
    /**
     * @return AddToCart
     */
    public static function getInstance(): AddToCart
    {
        if (is_null(self::$instance)) {
            self::$instance = new AddToCart();
        }
        
        return self::$instance;
    }
    
    /**
     * Add a product to the customer shopping cart.
     *
     * @return void
     */
    public function addToCart()
    {
        $bddService = ProductService::getInstance();
        
        $nb = 0;
        if (isset($_POST['nbProduct'])) {
            $nb = $_POST['nbProduct'];
        }
        if (!isset($_SESSION['count'])) {
            $_SESSION['count'] = $nb;
        } else {
            $_SESSION['count'] += $nb;
        }
        $product_count = $_SESSION['count'];
        
        if (isset($_POST['product_id'])) {
            $product_id = $_POST['product_id'];
            $product = $bddService->searchById($product_id);
            $productInfo = $product->fetch(PDO::FETCH_ASSOC);
            
            if (!is_array($productInfo)) {
                throw new Exception("productInfo is not a array");
            }

            $productInfo['demand_quantity'] = $nb;
            
            if (!isset($_SESSION['productsInShoppingCart'])) {
                $_SESSION['productsInShoppingCart'] = array($productInfo['id'] => $productInfo);
            } else {
                if (isset($_SESSION['productsInShoppingCart'][$productInfo['id']])) { /* si le produit est deja dans le panier */
                    $_SESSION['productsInShoppingCart'][$productInfo['id']]['demand_quantity'] += $nb;
                } else {
                    $_SESSION['productsInShoppingCart'][$productInfo['id']] = $productInfo;/* ajoute un objet dans le tableau id => $productInfo*/
                }
            }
            $productsInShoppingCart = $_SESSION['productsInShoppingCart'];
        }
        
        require('../view/shoppingCartPage.php');
    }
}

<?php

require_once "../service/ProductService.php";

class ProductSheet
{

    private static ?ProductSheet $instance = null;
    
    private function __construct()
    {
    }
    
    /**
     * @return ProductSheet
     */
    public static function getInstance(): ProductSheet
    {
        if (is_null(self::$instance)) {
            self::$instance = new ProductSheet();
        }
        
        return self::$instance;
    }
    
    /**
     * Load a product page its id.
     *
     * @return void
     */
    public function productSheetForm()
    {
        $bddService = ProductService::getInstance();
        
        $productId = $_GET['product_id'];
        $product = $bddService->searchById($productId);
        $productInfo = $product->fetch(PDO::FETCH_ASSOC);
    
        if (!is_array($productInfo)) {
            throw new Exception("productInfo is not a array");
        }
        
        $quantite_deja_demande = 0;
        if (isset($_SESSION['productsInShoppingCart'][$productInfo['id']])) {
            $quantite_deja_demande = $_SESSION['productsInShoppingCart'][$productInfo['id']]['demand_quantity'];
        }
        
        require('../view/productPage.php');
    }
}

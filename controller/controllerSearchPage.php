<?php

require_once "../service/ProductService.php";

class SearchPage
{

    private static ?SearchPage $instance = null;
    
    private function __construct()
    {
    }
    
    /**
     * @return SearchPage
     */
    public static function getInstance(): SearchPage
    {
        if (is_null(self::$instance)) {
            self::$instance = new SearchPage();
        }
        
        return self::$instance;
    }
    
    /**
     * Search all products in the database.
     *
     * @return void
     */
    public function searchPage()
    {
        $bddService = ProductService::getInstance();
        
        $products = $bddService->getAllProducts();
        
        require('../view/searchPage.php');
    }
}

@servers(['web' => 'root@161.35.93.20'])

@setup
    $repository = 'git@gitlab.com:lucas.montagnier32/site-web-test.git';
    $releases_dir = '/opt/lucas/site-web-test/releases';
    $app_dir = '/opt/lucas/site-web-test';
    $release = date('YmdHis');
    $new_release_dir = $releases_dir .'/'. $release;
@endsetup


@story('deploy')
    clone_repository
    run_composer
    run_yarn
    update_links
@endstory


@task('clone_repository')
    echo 'Cloning repository'
    [ -d {{ $releases_dir }} ] || mkdir {{ $releases_dir }}
    git clone --depth 1 {{ $repository }} {{ $new_release_dir }}
@endtask

@task('run_composer')
    echo "Composer install ({{ $release }})"
    cd {{ $new_release_dir }}
    composer install --prefer-dist --no-scripts -q -o
@endtask

@task('run_yarn')
    echo "Yarn install ({{ $release }})"
    cd {{ $new_release_dir }}

    chmod 777 log/
    (echo "Version du " && TZ="Europe/Paris" date %d% && echo "(heure de Paris)")
    touch VERSION
    (echo "Version du " && TZ="Europe/Paris" date && echo "(heure de Paris)") > VERSION

    yarn install
    ./node_modules/webpack/bin/webpack.js
@endtask

@task('update_links')
    echo 'Linking current release'
    rm {{ $app_dir }}/current/*
    ln -nfs {{ $new_release_dir }}/* {{ $app_dir }}/current
@endtask

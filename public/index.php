<?php
require_once '../service/Logger.php';

session_start();

//Chargement de l'autoloader
require_once '../Autoloader.php';
Autoloader::autoloadRegisterController();

if (isset($_GET['action'])) {
//  naming example: if ($_GET['action'] == 'homePage') { HomePage::getInstance()->homePage(); }
    $str = substr($_GET['action'],1);
    (strtoupper(substr($str,0,1)) . substr($str,1))::getInstance()->$str();
} else {
    if (isset($_GET['product_id'])){/* vers page du produit d'id product_id */
        ProductSheet::getInstance()->productSheetForm();
    } elseif (isset($_GET['array_index_to_remove'])) {
        RemoveProductFromCart::getInstance()->removeProductFromShoppingCart();
    } elseif (isset($_GET['remove_all'])) {
        RemoveAllCart::getInstance()->removeAllShoppingCart();
    } elseif (isset($_GET['confirm_cart'])) {
        ConfirmCartPage::getInstance()->toConfirmCart();
    } elseif (isset($_GET['selectTwoProduct'])) {
        SelectTwoProduct::getInstance()->selectTwoProduct();
    } else {
        $_SESSION = array();
        Logger::emptyLog();
        HomePage::getInstance()->homePage();
    }
}
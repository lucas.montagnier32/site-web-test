<?php

class Autoloader
{
    
    /**
     * @return void
     */
    static function autoloadRegisterController(){
        spl_autoload_register(array(__CLASS__, 'autoloadController'));
    }
    
    /**
     * @param $class string Le nom de la classe à charger
     */
    static function autoloadController(string $class){
        require 'controller/controller' . $class . '.php';
    }
}
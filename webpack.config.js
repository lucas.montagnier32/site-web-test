const webpack = require("webpack");
const path = require("path");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

module.exports = {
    entry: {
        HomePage: "./ressources/js/homePage.js",
        SearchPage: "./ressources/js/searchPage.js",
        ProductPage: "./ressources/js/productPage.js",
    },
    output: {
        path: path.resolve(__dirname, "./public/dist"),
        filename: "./[name].js"
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: "babel-loader"
            },
            {
                test: /\.s[ac]ss$/i,
                use: [
                    MiniCssExtractPlugin.loader,
                    "css-loader",
                    "sass-loader",
                ],
            },
        ]
    },
    plugins: [
        new MiniCssExtractPlugin({
            // Options similar to the same options in webpackOptions.output
            // both options are optional
            filename: "style[name].css",
            chunkFilename: "[id].css",
        }),
    ],
}
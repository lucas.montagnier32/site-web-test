### Installation avec docker-compose:
Cloner dans le répertoire ```/opt/dstock/lucas/``` avec:

```git clone git@gitlab.com:lucas.montagnier32/site-web-test.git .```

Attribuer les permissions au fichier de log:

```chmod 777 log/logFile.log```

Installer les dépendances et exécuter webpack:

```sudo apt install npm && npm install --global yarn && yarn install && ./node_modules/webpack/bin/webpack.js```

Puis exécuter docker-compose dans le répertoire ```/opt/dstock/lucas/docker/``` avec:

```sudo docker-compose build && sudo docker-compose up -d```

Accès par navigateur en local: ```http://localhost:3000```

Dans docker/nginx/nginx.conf:

fastcgi_pass php:9000 pour docker

fastcgi_pass 127.0.0.1:9000 pour Kubernetes 

### dbconfig
#### for Kubernetes cluster app:
```
<?php
return array(
    'host' => 'mysql-svc',
    'username' => 'root',
    'password' => 'root',
    'dbname' => 'dbtest',
);
```
#### for traditional app:
```
<?php
return array(
    'host' => 'localhost',
    'username' => 'superman',
    'password' => 'superman',
    'dbname' => 'dbtest',
);
```

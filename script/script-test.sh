#!/bin/bash

echo "script-test file";

nb_files=`ls public/ | wc -l`;

echo "$nb_files files in public folder";

if [ $nb_files != 1 ]; then
    echo "public folder must have one file";
    exit 1;
fi

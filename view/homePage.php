<!DOCTYPE html>
<?php require 'generateUrl.php' ?>
<html>
	<head>
		<meta charset="utf-8" />
		<link rel="stylesheet" href="<?= generateCssUrl('styleHomePage.css') ?>" />
		<title> site </title>
	</head>

	<body>
		<div id="bloc_page">
	 		<header>
				<h1> Site de Lucas Montagnier </h1>
				<nav>
		            <ul>
						<li><a id="here" href="#">Accueil</a></li>
						<li><a href="<?= generateActionUrl('searchPage')?>">Recherche</a></li>
                        <li><a href="<?= generateActionUrl('shoppingCartPage')?>">Panier</a></li>
		            </ul>
	        	</nav>
			</header>
	        
        	<section>
            <div>
                Bienvenue à tous<br>
            </div>
            <div id="timer"></div>
            
            <div id="conteneur">
			   <div id="element1"> </div>
			   <div id="element2"> </div>
			</div>
        	</section>
            <script src="<?= generateJsUrl('HomePage.js')?>"> </script>

        	<?php require('footerTemplate.php') ?>
    	</div>
   </body>
</html>
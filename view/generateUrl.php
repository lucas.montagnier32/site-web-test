<?php

function generateCssUrl(string $name): string
{
    return "/dist/" . $name;
}

function generateActionUrl(string $name): string
{
    return $name;
}

function generateJsUrl(string $name): string
{
    return "/dist/" . $name;
}
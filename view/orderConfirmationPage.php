<!DOCTYPE html>
<?php require 'generateUrl.php' ?>
<html>
    <head>
        <meta charset="utf-8" />
        <link rel="stylesheet" href="<?= generateCssUrl('styleHomePage.css') ?>" />
        <title> site </title>
    </head>
    
    <body>
        <div id="bloc_page">
            <header>
                <h1> Page de confirmation </h1>
                <nav>
                    <ul>
                        <li><a href="<?= generateActionUrl('homePage')?>">Accueil</a></li>
                        <li><a href="<?= generateActionUrl('searchPage')?>">Recherche</a></li>
                        <li><a href="<?= generateActionUrl('shoppingCartPage')?>">Panier</a></li>
                    </ul>
                </nav>
            </header>
            
            <section>
                <div>
                    commande confirmée email envoyé à <?php echo $userInfo['email']?>
                </div>
            </section>
            
            <?php require('footerTemplate.php') ?>
        </div>
    </body>
</html>

<!DOCTYPE html>
<?php require 'generateUrl.php' ?>
<html>
    <head>
        <meta charset="utf-8" />
        <link rel="stylesheet" href="<?= generateCssUrl('styleSearchPage.css') ?>" />
        <title> site </title>
    </head>
    
    <body>
        <div id="bloc_page">
            <header>
                <h1> Validation du panier </h1>
                <nav>
                    <ul>
                        <li><a href="<?= generateActionUrl('homePage')?>">Accueil</a></li>
                        <li><a href="<?= generateActionUrl('searchPage')?>">Recherche</a></li>
                        <li><a href="<?= generateActionUrl('shoppingCartPage')?>">Panier</a></li>
                    </ul>
                </nav>
            </header>
        
            <section id="checkout_section">
                <aside>
                    <div id="checkout_big_div">
                        <?php if (!$connected){?>
                            <div>
                                <fieldset>
                                    <legend>Créer un compte</legend>
                                    <form id="checkout_form" method="post" action="<?= generateActionUrl('createAccount')?>">
                                        Prénom:
                                        <input type="text" name="firstname" required>
                                        Nom:
                                        <input type="text" name="lastname" required>
                                        Mail:
                                        <input type="text" name="mail" required>
                                        Mot de passe:
                                        <input type="password" name="password" required>
                                        Adresse:
                                        <input type="text" name="address" required>
                                        Code postal:
                                        <input type="text" name="zipcode" required>
                                        Ville:
                                        <input type="text" name="city" required>
                                        <input id="log_button" type="submit" name="create_account" value="Créer">
                                    </form>
                                    <?php if ($accountCreated){?>
                                        <div>
                                            Le compte a bien été créé
                                        </div>
                                    <?php } ?>
                                </fieldset>
                            </div>
                            <div>
                                <fieldset>
                                    <legend>Se connecter</legend>
                                    <form id="checkout_form" method="post" action="<?= generateActionUrl('login')?>">
                                        Mail:
                                        <input type="text" name="mail" required>
                                        Mot de passe:
                                        <input type="password" name="password" required>
                                        <input id="log_button" type="submit" name="log_account" value="Connection">
                                    </form>
                                </fieldset>
                            </div>
                        <?php }; ?>
                    </div>
                </aside>
                <div id="right">
                    <?php if ($connected){?>
                        <div>
                            Bonjour <?php echo $userInfo['firstname']; echo " "; echo $userInfo['lastname'];?> , votre panier:
                        </div>
                        <div>
                            <table>
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Nom</th>
                                    <th>Quantité Dispo</th>
                                    <th>Prix HT</th>
                                    <th>Quantité demandé</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $total=0;
                                foreach ($productsInShoppingCart as $key => $product){
                                    $total+= $product['price_ht']*$product['demand_quantity']  ?>
                                    <tr>
                                        <td><?php echo htmlspecialchars($product['id']); ?></td>
                                        <td><?php echo htmlspecialchars($product['name']); ?></td>
                                        <td><?php echo htmlspecialchars($product['quantity']); ?></td>
                                        <td><?php echo htmlspecialchars($product['price_ht']); ?></td>
                                        <td><?php echo htmlspecialchars($product['demand_quantity']); ?></td>
                                    </tr>
                                <?php }; ?>
                                </tbody>
                            </table>
                        </div>
                        <div>
                            Prix d'achat HT: <?php echo $total ?> €
                        </div>
                        <div>
                            Prix d'achat TTC: <?php $TVA=5; echo number_format($total*(($TVA/100)+1),2)?> €
                        </div>
                        <div>
                            Adresse: <br>
                            <?php echo $userInfo['street']; ?> <br>
                            <?php echo $userInfo['zipcode']." ".$userInfo['city']; ?> <br>
                        </div>
                        <div>
                            Un email sera envoyé à: <?php echo $userInfo['email'] ?>
                        </div>
                        <hr>
                        <div>
                            <form method="post" action="<?= generateActionUrl('confirmOrder') ?>">
                                <input id="confirm_order_button" type="submit" name="submit_confirm_order" value="Valider la commande">
                            </form>
                        </div>
                    <?php } else { ?>
                        <div>
                            Vous n'êtes pas connecté
                        </div>
                    <?php } ?>
                </div>
            </section>
            
            <?php require('footerTemplate.php') ?>
        </div>
    </body>
</html>

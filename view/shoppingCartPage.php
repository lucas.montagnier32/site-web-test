<!DOCTYPE html>
<?php require 'generateUrl.php' ?>
<html>
    <head>
        <meta charset="utf-8" />
        <link rel="stylesheet" href="<?= generateCssUrl('styleHomePage.css') ?>" />
        <title> site </title>
    </head>
    
    <body>
    <div id="bloc_page">
        <header>
            <h1>Panier</h1>
            <nav>
                <ul>
                    <li><a href="<?= generateActionUrl('homePage')?>">Accueil</a></li>
                    <li><a href="<?= generateActionUrl('searchPage')?>">Recherche</a></li>
                    <li><a id="here" href="#">Panier</a></li>
                </ul>
            </nav>
        </header>
        
        <section id="shoppingCartSection">
			<?php if ($product_count>0) { ?>
				<div id="leftRightDiv">
                    <fieldset>
                        <legend>
                            <?php echo $product_count;
                            if ($product_count>1) { ?> produits
                            <?php } else { ?>
                                produit
                            <?php } ?> dans le panier
                        </legend>
                        <div id="leftPartDiv">
                            <table>
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Nom</th>
                                    <th>Quantité Dispo</th>
                                    <th>Prix HT</th>
                                    <th>Quantité demandé</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $total=0;
                                foreach ($productsInShoppingCart as $key => $product){
                                    $total+= $product['price_ht']*$product['demand_quantity']  ?>
                                    <tr>
                                        <td><?php echo htmlspecialchars($product['id']); ?></td>
                                        <td><?php echo htmlspecialchars($product['name']); ?></td>
                                        <td><?php echo htmlspecialchars($product['quantity']); ?></td>
                                        <td><?php echo htmlspecialchars($product['price_ht']); ?></td>
                                        <td><?php echo htmlspecialchars($product['demand_quantity']); ?></td>
                                        <td>
                                            <form id="removeButton" method="get" action="supprimer" >
                                                <input type="hidden" name="array_index_to_remove" value="<?php echo $key; ?>">
                                                <button>Supprimer</button>
                                            </form>
                                        </td>
                                    </tr>
                                <?php }; ?>
                                </tbody>
                            </table>
                        </div>
                        <div id="leftPartDiv">
                            Total: <?php echo $total ?> € HT
                        </div>
                    </fieldset>
                    <div>
                        <form method="post" action="<?= generateActionUrl('searchPage')?>">
                            <button>Rechercher d'autres produits</button>
                        </form>
                    </div>
				</div>
				<div id="leftRightDiv">
                    <div>
                        <form id="rightButton" method="get" action="vider">
                            <input type="hidden" name="remove_all">
                            <button id="empty" >Vider le panier</button>
                        </form>
                    </div>
                    <hr>
                    <div>
                        <form id="rightButton" method="get" action="confirmer">
                            <input type="hidden" name="confirm_cart">
                            <button id="confirm" >Valider le panier</button>
                        </form>
                    </div>
				</div>
				<?php
			} else {?>
                <div id="empty_cart">
                    <div>
                        Votre panier est vide
                    </div>
                    <div>
                        <form method="post" action="<?= generateActionUrl('searchPage')?>">
                            <button>Rechercher des produits</button>
                        </form>
                    </div>
                </div>
            <?php
			}
			?>
		</section>

		<?php require('footerTemplate.php') ?>
    </div>
    </body>
</html>
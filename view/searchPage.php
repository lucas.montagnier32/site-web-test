<!DOCTYPE html>
<?php require 'generateUrl.php' ?>
<html lang="">
	<head>
        <meta charset="utf-8" />
		<link rel="stylesheet" href="<?= generateCssUrl('styleSearchPage.css') ?>" />
		<title> site </title>
	</head>

	<body>
		<div id="bloc_page">
	 		<header>
				<h1> Rechercher des produits </h1>
				<nav>
		            <ul>
                        <li><a href="<?= generateActionUrl('homePage')?>">Accueil</a></li>
						<li><a id="here" href="#">Recherche</a></li>
                        <li><a href="<?= generateActionUrl('shoppingCartPage')?>">Panier</a></li>
		            </ul>
	        	</nav>
			</header>
	        
        	<section>
				<div id="left">
     
					<form id="searchForm" method="post" action="<?= generateActionUrl('searchByName') ?>">
						<div id="search_name">
							<div id="text_input">
								<input class="text_input" type="text" name="search_name" id="product_name" placeholder="Trouver des produits... "/>
							</div>
							<div id="search_submit">
								<input id="search_input" class="search_submit" type="submit" name="submit_name" value="Rechercher">
							</div>
						</div>
					</form>
				

					<div id="cards">
                        <?php $row = $products->fetch(PDO::FETCH_ASSOC); $count=0;
                        	while($row !=null) : ?>
							<div id="lineCards">
								<?php
									$mod = 0;
									while($mod<4 && $row!=null) : ?>
									<div id="card">
										<div>image</div>
										<div id="name"><?php echo htmlspecialchars($row['name']); ?></div>
										<div><?php echo htmlspecialchars($row['quantity']); ?> pièces</div>
										<div id="price"><?php echo htmlspecialchars($row['price_ht']); ?> € HT</div>
										<div id="name">
											<form id="fiche_button" method="get" action="produit">
												<input type="hidden" name="product_id" value="<?php echo htmlspecialchars($row['id']); ?>">
												<input id="search_input" type="submit" value="voir fiche">
											</form>
										</div>
									</div>
								<?php $mod +=1; $row = $products->fetch(PDO::FETCH_ASSOC); $count++; endwhile;?>
							</div>
						<?php endwhile; ?>
					</div>
				</div>
				
				<aside id="searchPageAside">
                    <?php echo htmlspecialchars($count); ?> résultats
					<form id="searchForm" method="post" action="<?= generateActionUrl('searchByPrice') ?>">
						<div id="search_price">
							<fieldset>
								<legend>Rechercher par prix:</legend>
								<div>
									<label for="prix1"> <5</label>
									<input type="radio" name="prix" id="prix1" value=1 />
								</div>
								<div>
									<label for="prix2"> <20</label>
									<input type="radio" name="prix" id="prix2" value=2 />
								</div>
								<div>
									<label for="prix3"> <50</label>
									<input type="radio" name="prix" id="prix3" value=3 />
								</div>
								<div>
									<label for="prix4"> <200</label>
									<input type="radio" name="prix" id="prix4" value=4 />
								</div>
								<div>
									<label for="prix5"> <500</label>
									<input type="radio" name="prix" id="prix5" value=5 />
								</div>
								<div>
									<label for="prix6"> tous</label>
									<input type="radio" name="prix" id="prix6" value=6 checked=""/>
								</div>
							</fieldset>
							<fieldset>
								<legend>Trier par:</legend>
								<div>
									<label for="sort1"> Nom</label>
									<input type="radio" name="sort" id="sort1" value="name" checked=""/>
								</div>
								<div>
									<label for="sort2"> Quantité</label>
									<input type="radio" name="sort" id="sort2" value="quantity" />
								</div>
								<div>
									<label for="sort3"> Prix</label>
									<input type="radio" name="sort" id="sort3" value="price_ht" />
								</div>
							</fieldset>
							<div><input id="search_input" class="search_submit" type="submit" name="submit" value="Rechercher" /></div>
						</div>
					</form>
					<div>
                        <?php
                        if (isset($_SESSION['count'])) {
                            $product_count = $_SESSION['count'];
                            if ($product_count>0) {
                                echo $product_count;
                                echo " produits dans le panier";
                            }
                            else {
                                echo "Votre panier est vide";
                            }
                        }
                        else{
                            echo "Votre panier est vide";
                        }
                        ?>
					</div>
				</aside>
        	</section>
            
            <?php require('footerTemplate.php') ?>
    	</div>
   </body>
</html>
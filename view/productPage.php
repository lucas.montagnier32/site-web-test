<!DOCTYPE html>
<?php require 'generateUrl.php' ?>
<html>
    <head>
        <meta charset="utf-8" />
        <link rel="stylesheet" href="<?= generateCssUrl('styleProductPage.css') ?>" />
        <title> site </title>
    </head>
    
    <body>
        <div id="bloc_page">
            <header>
                <h1>Fiche du produit <?php echo htmlspecialchars($productInfo['name']); ?></h1>
                <nav>
                    <ul>
                        <li><a href="<?= generateActionUrl('homePage')?>">Accueil</a></li>
                        <li><a href="<?= generateActionUrl('searchPage')?>">Recherche</a></li>
                        <li><a href="<?= generateActionUrl('shoppingCartPage')?>">Panier</a></li>
                    </ul>
                </nav>
            </header>
            
            <section>
				<div id="bigDiv">
					<div id="leftRightDiv">
						<div id="leftDiv">
							<div>image</div>
							<div>
                                <?php echo htmlspecialchars($productInfo['description']); ?>
							</div>
						</div>
						<div id="rightDiv">
							<div id="price"><?php echo htmlspecialchars($productInfo['price_ht']); ?> € HT</div>
							<div>id: <?php echo htmlspecialchars($productInfo['id']); ?></div>
							<div>
								<script>
                                    let quantity = '<?=$productInfo['quantity']?>';
                                    if (quantity > 1) {
                                        document.write("Plus que "+quantity+"  pièces disponibles");
                                    } else if (quantity == 1) {
                                        document.write("Plus que "+quantity+"  pièce disponible");
                                    } else {
                                        document.write("Cette pièce n'est plus disponible");
									}
								</script>
							</div>
							<div>
								<form id="addToCart" method="post" action="<?= generateActionUrl('addToCart')?>">
									<input type="hidden" name="product_id" value="<?php echo htmlspecialchars($productInfo['id']); ?>">
									<input type="number" name="nbProduct" value="1" min="1" max="<?=$productInfo['quantity']-$quantite_deja_demande?>">
									<input id="button" type="submit" value="Ajouter au panier">
								</form>
							</div>
						</div>
					</div>
				</div>
            </section>
    
            <?php require('footerTemplate.php') ?>
        </div>
    </body>
</html>

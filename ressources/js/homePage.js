import '../scss/style.scss';

function selectTwoProduct(){
    const xhttp = new XMLHttpRequest();
    xhttp.onload = function() {
        document.getElementById("element1").innerHTML = this.responseText.split("\n")[0];
        document.getElementById("element2").innerHTML = this.responseText.split("\n")[1];
    }
    xhttp.open("GET", "index.php?selectTwoProduct");
    xhttp.send();
}

let time = 5;
const timerElement = document.getElementById("timer");
timerElement.innerText = time;
selectTwoProduct();

function decreaseTimer() {
    if (time == 0) {
        time = 5;
        selectTwoProduct();
    } else {
        time--;
    }
    timerElement.innerText = time;
}
setInterval(decreaseTimer, 1000);